import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class GithubService {
    private username: string;
    private clientID: string;
    private clientSecret: string;

    constructor(private _http: Http){
        this.clientID = "215407e47749a3e52bc2";
        this.clientSecret = "d1a6420bf6dfb0b3f19c2f0858423e39118ca0a3";
    }

    getUser(){
        return this._http.get('http://api.github.com/users/' + this.username + "?client_id=" + this.clientID + "&client_secret=" + this.clientSecret)
        .map(res => res.json())  
    }

    getRepos(){
        return this._http.get('http://api.github.com/users/' + this.username + "/repos?client_id=" + this.clientID + "&client_secret=" + this.clientSecret)
        .map(res => res.json())  
    }

    updateUser(username: string){
        this.username = username;
    }
}